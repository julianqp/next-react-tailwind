import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="es">
        <Head>
          <meta charSet="utf-8" />
          <meta name="description" content="Descripción" key="description" />
          <meta name="keywords" content="palabra clave 1, palabra clave 2, palabra clave 3" />
          <meta name="author" content="Julián Querol Polo" />
          <meta name="copyright" content="Julián Querol Polo" />
        </Head>
        <body id="body" className="w-full">
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
