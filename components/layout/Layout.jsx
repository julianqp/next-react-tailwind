import PropTypes from 'prop-types'
import Footer from './Footer'
import Header from './Header'

const Layout = ({ children }) => (
  <div className="flex flex-col min-h-screen">
    <Header />
    <main className="flex-grow container mx-auto">{children}</main>
    <Footer />
  </div>
)

Layout.propTypes = {
  children: PropTypes.element,
}

Layout.defaultProps = {
  children: null,
}
export default Layout
