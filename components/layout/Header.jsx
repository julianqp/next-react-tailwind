import Head from 'next/head'

const Header = () => (
  <header className="bg-gray-100 sticky top-0 w-full">
    <Head>
      <title>Nuevo proyecto</title>
    </Head>
    <div className="p-3 flex items-center justify-between">
      <h1>Logo</h1>
      <h1>Menu</h1>
      <h1>Sesion</h1>
    </div>
  </header>
)

export default Header
